javascript: (function (e) {
	function getNewOrderArray(length) {
		var orderArr = new Array(length);
		var i = 0;
		var shuffleStartIndex = 20;
		var shuffleEndIndex = length - 1;
		if ((shuffleEndIndex + 1) >= length) {
			shuffleEndIndex = length - 1;
		}
		for (i = 0; i < length; i++) {
			orderArr[i] = i + 1;
		}
		for (i = shuffleStartIndex; i < shuffleEndIndex; i++) {
			var randomIndex = Math.floor(Math.random() * ((length - 1) - shuffleStartIndex + 1) + shuffleStartIndex);
			var temp = orderArr[randomIndex];
			orderArr[randomIndex] = orderArr[i];
			orderArr[i] = temp;
		}
		console.log(orderArr);
		return orderArr;
	}
	function iterate(newOrderArray, movies, i, errorCount) {
		i = i + 1;
		if (i == newOrderArray.length || window.cancel === true) {
			alert('Your queue has been shuffled.  Refresh the page to see the new order.');
			return;
		}
		if (errorCount === 5) {
			console.error('Too many errors... canceling');
			return;
		}
		var currMovie = movies[i];
		var mid = $(currMovie).attr('data-id');
		var title = $(currMovie).attr('aria-label').substring(14);
		var pos = newOrderArray[i];
		if (pos !== i + 1) {
			var mainUrl = 'https://portal.dvd.netflix.com/queue/move?';
			var theUrl = mainUrl + 'position=' + pos + '&titleId=' + mid;
			var request = $.ajax({
					method: "POST",
					url: theUrl,
					dataType: 'text',
					xhrFields: {
						withCredentials: true
					}
				});
			request.done(function (data) {
				var dataJson = JSON.parse(data);
				if (dataJson.status !== 'MOVED') {
					i = i - 1;
					errorCount = errorCount + 1;
					console.warn('Error moving "' + title + '" to position ' + pos);
					console.warn('Waiting 5 seconds to try again...');
					setTimeout(function () {
						iterate(newOrderArray, movies, i, errorCount)
					}, 5000);
				} else {
					errorCount = 0;
					console.info('"' + title + '" was moved to position ' + pos);
					setTimeout(function () {
						iterate(newOrderArray, movies, i, errorCount)
					}, 100);
				}
			});
		} else {
			console.info('"' + title + '" is already at position ' + pos);
			iterate(newOrderArray, movies, i, errorCount);
		}
	}
	var orgArr = $('.ui-sortable-handle');
	var orderArr = getNewOrderArray(orgArr.length);
	window.cancel = false;
	iterate(orderArr, orgArr, -1, 0);
})(this)
