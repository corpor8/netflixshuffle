Netflix Shuffle Bookmarklet
=============================

## Installation
Add a new book mark and paste code into URL property

you can change the shuffleStartIndex property in the code to where you want the shuffling to start.  By default it will shuffle everything after the top 20.

## Usage
Navigate to dvd queue page and click the bookmarklet.  It should take between 200-300 ms for each item in the queue.  An alert will display when it is finished.  Refresh the page to see the updated queue.